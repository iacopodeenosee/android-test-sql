package idnlab.iacopodeenosee.test_sql;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_TOUCH_MASK = "input";
    public static final String ID = "_id";
    public static final String SCREEN = "screen";
    public static final String TYPE = "type";
    public static final String ORIENTATION = "orientation";
    public static final String NUMBER_TOUCH = "number_touch";
    public static final String COMMENT = "comment";

    //private static final String DATABASE_NAME = "test_PIM.sqlite";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_TOUCH_MASK + " ( "
            + ID + " integer primary key autoincrement, "
            + SCREEN + " integer, "
            + TYPE + " text not null, "
            + ORIENTATION + " integer, "
            + NUMBER_TOUCH + " integer, "
            + COMMENT + " text not null);";
    private static String DB_NAME = "test_PIM.sqlite";
    private static String DB_PATH = "/data/data/" + BuildConfig.APPLICATION_ID + "/databases/";
    public SQLiteDatabase tmpDB;
    private Context DBcontext;

    public DBHelper(Context context) throws IOException, SQLException {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.DBcontext = context;
        boolean dbexist = checkdatabase();
        if (dbexist) {
            opendatabase();
        } else {
            createdatabase();
        }

    }

    @Override
    public void onCreate(SQLiteDatabase database) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOUCH_MASK);
        onCreate(db);
    }

    public void createdatabase() throws IOException {
        boolean dbexist = checkdatabase();
        if (dbexist) {
            Log.v("test_SQL", "DB exist");
        } else {
            Log.v("test_SQL", "DB create connection");
            this.getReadableDatabase();
            try {
                copydatabase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkdatabase() {
        Log.v("test_SQL", "check DB");
        boolean checkdb = false;
        try {
            String myPath = DB_PATH + DB_NAME;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        } catch (SQLiteException e) {
            Log.v("test_SQL", "DB NOT present");
        }
        return checkdb;
    }

    private void copydatabase() throws IOException {
        Log.v("test_SQL", "copy DB to database folder");
        InputStream myinput = DBcontext.getAssets().open(DB_NAME);
        String outfilename = DB_PATH + DB_NAME;
        OutputStream myoutput = new FileOutputStream(outfilename);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = myinput.read(buffer)) > 0) {
            myoutput.write(buffer, 0, length);
        }

        myoutput.flush();
        myoutput.close();
        myinput.close();
    }

    public void opendatabase() throws SQLException {
        Log.v("test_SQL", "open DB");
        String mypath = DB_PATH + DB_NAME;
        tmpDB = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READONLY);
    }

    public synchronized void close() {
        if (tmpDB != null) {
            tmpDB.close();
        }
        super.close();
    }
}