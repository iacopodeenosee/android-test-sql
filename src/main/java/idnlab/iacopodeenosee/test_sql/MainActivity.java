package idnlab.iacopodeenosee.test_sql;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    String strQueryContent = "";
    private Button btnLoadSql, btnGetActiveProfile;
    private TextView tvwSqlContent;
    private Spinner spn_input;
    private EditText edt_numberT;
    private DataSource tmpDS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.v("test_SQL", "load main activity");

        tvwSqlContent = findViewById(R.id.tvw_content);

        spn_input = findViewById(R.id.spn_typeinput);
        edt_numberT = findViewById(R.id.etw_numberT);

        //setup spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.input,
                android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_input.setAdapter(adapter);

        //setup button load sql
        btnLoadSql = findViewById(R.id.btn_loadSQL);
        btnLoadSql.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.v("test_SQL", "press button load db");

                //load local DB sqlite
                LoadDB("test_PIM.sqlite");
            }

        });

        //setup button get active profile
        btnGetActiveProfile = findViewById(R.id.btn_getActiveProfile);
        btnGetActiveProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Log.v("test_SQL", "press button get active profile");

                //get info from device
                String varType = spn_input.getSelectedItem().toString();
                int varNumberT = Integer.parseInt(edt_numberT.getText().toString());
                int orientation = getResources().getConfiguration().orientation;
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                MatrixData.getInstance().updateData(varType, orientation,
                        varNumberT, metrics);

                strQueryContent = getActiveProfileDB();

                if (strQueryContent.equals("")) {
                    Toast.makeText(view.getContext(), "QUERY DB EMPTY", Toast.LENGTH_LONG);
                } else {
                    String pkg = getPackageName();
                    Intent i = new Intent(getApplicationContext(), sqlQueryActivity.class);

                    i.putExtra(pkg + ".strQuery", strQueryContent);

                    view.getContext().startActivity(i);
                }
            }

        });
    }

    private boolean LoadDB(String file) {
        boolean result = false;

        Log.v("test_SQL", "create DB");
        tmpDS = new DataSource(this);
        tmpDS.open();

        List<TouchMask> values = tmpDS.getAllTouchMask();

        //get all touchmask present into DB
        Log.v("test_SQL", "number of element: " + values.size());

        String tmpDBcontent = "";
        tvwSqlContent.setTextColor(Color.RED);
        for (int i = 0; i < values.size(); i++) {
            Log.v("test_SQL", "load value:" + i);
            tmpDBcontent += values.get(i).getComment() + "\n";
        }
        tvwSqlContent.setText(tmpDBcontent);

        return result;
    }

    private String getActiveProfileDB() {
        TouchMask tmpTM = null;

        Log.v("test_SQL", "create DB");
        tmpDS = new DataSource(this);
        tmpDS.open();

        tmpTM = tmpDS.getActiveProfile(MatrixData.getInstance());

        return tmpTM.getComment();
    }
}