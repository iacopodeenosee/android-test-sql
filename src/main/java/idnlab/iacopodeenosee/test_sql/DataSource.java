package idnlab.iacopodeenosee.test_sql;


import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class DataSource {

    // Database fields
    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] allColumns = {DBHelper.ID, DBHelper.SCREEN, DBHelper.TYPE, DBHelper.ORIENTATION,
            DBHelper.NUMBER_TOUCH, DBHelper.COMMENT};

    public DataSource(Context context) {
        Log.v("test_SQL", "create DBHelper");

        try {
            dbHelper = new DBHelper(context);
        } catch (IOException e) {
            Log.v("test_SQL", e.toString());
        } catch (java.sql.SQLException e) {
            Log.v("test_SQL", e.toString());
        }
    }

    public void open() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public List<TouchMask> getAllTouchMask() {
        Log.v("test_SQL", "get all record of element");

        List<TouchMask> listTM = new ArrayList<TouchMask>();

        Cursor cursor = database.query(DBHelper.TABLE_TOUCH_MASK,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            TouchMask TM = cursorToTM(cursor);
            listTM.add(TM);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return listTM;
    }

    public TouchMask getActiveProfile(MatrixData tmpMD) {
        TouchMask TM = null;

        String queryScreen = DBHelper.SCREEN + " >= " + String.valueOf(tmpMD.getInches());
        String queryType = DBHelper.TYPE + " = " + "'" + tmpMD.getTypeInput() + "'";
        String queryOrientation = DBHelper.ORIENTATION + " = "
                + String.valueOf(tmpMD.getOrientation());
        String queryNumberTouch = DBHelper.NUMBER_TOUCH + " = "
                + String.valueOf(tmpMD.getNumberT());
        String queryOrder = " order by " + DBHelper.SCREEN + " asc";

        String queryTM = queryScreen + " and " + queryType + " and " + queryOrientation + " and "
                + queryNumberTouch + queryOrder;

        Log.v("test_SQL", "query DB: " + queryTM);

        Cursor searchcursor = database.query(DBHelper.TABLE_TOUCH_MASK, new String[]{
                        DBHelper.ID, DBHelper.SCREEN, DBHelper.TYPE, DBHelper.ORIENTATION, DBHelper
                        .NUMBER_TOUCH, DBHelper.COMMENT},
                queryTM, null, null, null, null);

        if (searchcursor != null && !searchcursor.isAfterLast()) {
            Log.v("test_SQL", "result query not null");
            searchcursor.moveToFirst();

            TM = cursorToTM(searchcursor);
        }

        // make sure to close the cursor
        searchcursor.close();

        return TM;
    }

    private TouchMask cursorToTM(Cursor cursor) {
        Log.v("test_SQL", "extract touchmask from cursur");

        TouchMask tmpTM = new TouchMask();
        tmpTM.setId(cursor.getLong(0));
        tmpTM.setComment(cursor.getString(5));

        return tmpTM;
    }
}