package idnlab.iacopodeenosee.test_sql;

import android.util.DisplayMetrics;
import android.util.Log;

class MatrixData {
    private static MatrixData instance;

    private int widthScreen = 0;
    private int heightScreen = 0;
    private int densityDPI = 0;
    private float density = 0.0f;
    private String typeInput;
    private int orientation;
    private int numberT = 0;
    private double inches = 0.0d;

    private MatrixData() {
    }

    public static MatrixData getInstance() {
        Log.v("test_SQL", "get instance matrixdata");

        if (instance == null) {
            instance = new MatrixData();
        }
        return instance;
    }

    public void updateData(String t, int o, int n, DisplayMetrics m) {
        Log.v("test_SQL", "update matrixdata data");

        this.widthScreen = m.widthPixels;
        this.heightScreen = m.heightPixels;
        this.typeInput = t;
        this.orientation = o;
        this.numberT = n;

        this.densityDPI = m.densityDpi;
        this.density = m.density;

        this.inches = this.getInches(m);
    }

    public int getWidthScreen() {
        return widthScreen;
    }

    public void setWidthScreen(int widthScreen) {
        this.widthScreen = widthScreen;
    }

    public int getHeightScreen() {
        return heightScreen;
    }

    public void setHeightScreen(int heightScreen) {
        this.heightScreen = heightScreen;
    }

    public String getTypeInput() {
        return typeInput;
    }

    public void setTypeInput(String typeInput) {
        this.typeInput = typeInput;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getNumberT() {
        return numberT;
    }

    public void setNumberT(int numberT) {
        this.numberT = numberT;
    }

    public int getDensityDPI() {
        return densityDPI;
    }

    public void setDensityDPI(int densityDPI) {
        this.densityDPI = densityDPI;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public double getInches() {
        return inches;
    }

    public void setInches(double inches) {
        this.inches = inches;
    }

    public double getInches(DisplayMetrics m) {
        double wi = (double) m.widthPixels / (double) m.xdpi;
        double hi = (double) m.heightPixels / (double) m.ydpi;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);

        return Math.sqrt(x + y);
    }

    public String getFinishInches(double inches) {
        String tmpF = "test_PIM_";
        if (inches <= 4) {
            tmpF += "4.xml";
        } else if (inches <= 6) {
            tmpF += "6.xml";
        } else if (inches <= 10) {
            tmpF += "10.xml";
        } else if (inches <= 17) {
            tmpF += "17.xml";
        } else {
            //ERROR
        }
        return tmpF;
    }
}

