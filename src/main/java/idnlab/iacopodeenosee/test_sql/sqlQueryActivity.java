package idnlab.iacopodeenosee.test_sql;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class sqlQueryActivity extends AppCompatActivity {

    TextView tvw_activeprofile, tvw_params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sql_query);

        Log.v("test_SQL", "load SQL result activity");

        Intent intent = getIntent();

        String pkg = getPackageName();
        String strQuery = intent.getStringExtra(pkg + ".strQuery");
        Log.v("test_SQL", "active profile:" + strQuery);

        tvw_activeprofile = findViewById(R.id.tvw_activeprofile);

        tvw_params = findViewById(R.id.tvw_params);

        final MatrixData profile = MatrixData.getInstance();
        String Ori;
        if (profile.getOrientation() == 1) {
            Ori = "portrait";
        } else {
            Ori = "landscape";
        }

        String tmpParams = tvw_params.getText() + "\nH: " +
                profile.getHeightScreen() + " | W: " +
                profile.getWidthScreen() + "\nDPI: " +
                profile.getDensityDPI() + " | d: " +
                profile.getDensity() + " | I: " +
                profile.getInches() + "\nO: " +
                Ori + "\nT: " +
                profile.getTypeInput() + "\nN: " +
                profile.getNumberT();

        tvw_params.setText(tmpParams);

        tvw_activeprofile.setText("ACTIVE PROFILE:\n" + strQuery);
    }
}

